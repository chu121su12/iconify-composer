import nodeFs from 'fs';
import fs from 'graceful-fs';

fs.gracefulify(nodeFs);

const assetFolder = 'icons/';

const excludes = [
	"cbi", // license
	"dashicons", // license
	"emojione-monotone", // emoji
	"emojione-v1", // emoji
	"emojione", // emoji
	"et", // license
	"fluent-emoji", // emoji
	"fxemoji", // emoji
	"gala", // license
	"gridicons", // license
	"icomoon-free", // license
	"noto-v1", // emoji
	"noto", // emoji
	"openmoji", // emoji
	"ps", // license
	"streamline-emojis", // emoji
	"twemoji", // emoji
];

(async () => {
	const icons = await fetch('https://raw.githubusercontent.com/iconify/icon-sets/master/collections.json')
		.then(x => x.json());

	for (let icon of Object.keys(icons)) {
		if (excludes.includes(icon)) {
			console.log(`- ${icon} skipped`);
			continue;
		}

		const url = `https://raw.githubusercontent.com/iconify/icon-sets/master/json/${icon}.json`;
		let i = 5;
		while (i-- > 0) {
			try {
				await fetch(url)
					.then(x => x.text())
					.then(x => fs.writeFileSync(`${assetFolder}${icon}.json`, x));

				console.log(`+ ${icon} downloaded`);

				break;
			}
			catch (e) {
				console.log(`- ${icon} error(${i}): ${e}`)
			}
		}
	}
})();
